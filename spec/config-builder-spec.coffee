describe "The ConfigBuilder", ->
  fs = require "fs"
  path = require "path"
  Promise = require "bluebird"
  mkdir = Promise.promisify require "mkdirp"
  rmdir = Promise.promisify require "rimraf"
  merge = require "deepmerge"
  #defaults = require("../src/default-settings")
  defaults = {}

  ConfigBuilder = require "../src/config-builder"
  ConfigNode = require "../src/config-node"

  tmpDir = fileA = fileB = typeDirA = typeDirB = undefined
  beforeEach ->
    tmpDir = tmpFileName()
    typeDirA = path.join tmpDir, "types-a"
    typeDirB = path.join tmpDir, "types-b"
    fileA = path.join tmpDir, "fileA.yaml"
    fileB = path.join tmpDir, "fileB.yaml"
    mkdir tmpDir
      .then ->
        fs.writeFileSync fileA, """
        ---
        port: 4242
        """
      .then ->mkdir typeDirA
      .then ->mkdir typeDirB
      .then ->
        fs.writeFileSync path.join(typeDirA, "foo.coffee"), """
          module.exports = "Foo"
        """
        fs.writeFileSync path.join(typeDirA, "bar.coffee"), """
          module.exports = "Bar"
        """
        fs.writeFileSync path.join(typeDirA, "bar.coffee"), """
          module.exports = "BarBar"
        """
        fs.writeFileSync path.join(typeDirA, "baz.coffee"), """
          module.exports = "Baz"
        """

  it "builds configuration objects", ->
    expect(ConfigBuilder().build()._options).to.eql defaults

  it "can load yaml config files", ->
    expect(
      ConfigBuilder()
        .tryLoad fileB
        .load fileA
        .build()._options
    ).to.eql merge defaults, port: 4242, __files:[fileA]

  describe "when adding options", ->
    xit "can resolve placeholders", ->
      expect(
        ConfigBuilder()
          .add __env:PUBLIC_IP: "123.456.78.9"
          .add ["PUBLIC_IP"], (PUBLIC_IP)->host:PUBLIC_IP
          .build()

      ).to.eql merge defaults,
        host: "123.456.78.9"
        __env: PUBLIC_IP: "123.456.78.9"
        __usedEnvVars: ["PUBLIC_IP"]

    it "allows ConfigNodes to customize the merging behaviour", ->
      class MyNode extends ConfigNode
        merge: (old)->
          nada: true

      oldSettings = foo:bar: new MyNode bang:baz: new MyNode oink:42
      newSettings = foo:bar: new MyNode bang:baz: new MyNode oink:45


      cfg = ConfigBuilder().add(oldSettings).add(newSettings).build()

      expect(cfg.foo).to.eql bar:nada:true



  describe "when loading files", ->
    it "can resolve placeholders in filenames", ->
      expect(
        ConfigBuilder()
          .add __env:HOME:tmpDir
          .load ["HOME"], (HOME)->path.join HOME,"fileA.yaml"
          .build()._options
      ).to.eql merge defaults,
        port: 4242,
        __files:[fileA]
        __env: HOME: tmpDir
        __usedEnvVars: ["HOME"]

    it "can parse placeholder names form callback arguments", ->
      expect(
        ConfigBuilder()
          .add __env:HOME:tmpDir
          .load (HOME)->path.join HOME,"fileA.yaml"
          .build()._options
      ).to.eql merge defaults,
        port: 4242,
        __files:[fileA]
        __env: HOME: tmpDir
        __usedEnvVars: ["HOME"]

    it "allows setting the search path for config types", ->
      cfg = ConfigBuilder()
          .typePath  typeDirA,typeDirB
          .build()._options
      expect(cfg).to.have.property("__typePath").eql([typeDirA,typeDirB])

      # note that these types would be constructors/classes, not strings.
      # I use strings to make testing easier.
      expect(cfg).to.have.property("__types").eql
        foo: "Foo"
        bar: "BarBar"
        baz: "Baz"

    it "will merge the results, if more than one search path is given", ->
      cfg = ConfigBuilder()
          .typePath typeDirA
          .typePath typeDirB
          .build()._options
      expect(cfg).to.have.property("__typePath").eql([typeDirA,typeDirB])

      expect(cfg).to.have.property("__types").eql
        foo: "Foo"
        bar: "BarBar"
        baz: "Baz"
    it "allows to manualy register/override type constructors", ->
      cfg = ConfigBuilder()
          .typePath typeDirA
          .types
            bar: "BarBar"
            baz: "Baz"
          .build()._options
      expect(cfg).to.have.property("__typePath").eql([typeDirA])

      expect(cfg).to.have.property("__types").eql
        foo: "Foo"
        bar: "BarBar"
        baz: "Baz"
